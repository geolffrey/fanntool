#include "fann.h"
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "include/k-means.h"


void k_means_partition_fann_train_data(struct k_means *km, struct fann_train_data *data) {

	int i, j;

	if (km->vector_size != data->num_input) {
		printf("Error: k_means must have same vector_size as the fann_train_data input vector.\n");
		return;
	}

	/* assign first k data vectors as the initial means */
	for (i=0; i<km->k; i++) {
		for (j=0; j<km->vector_size; j++) {
			km->means[i][j] = data->input[i%data->num_input][j];
		}
	}


	/* allocate memory for each data vector's centroid designation */
	int *centroid_indices = calloc(data->num_data, sizeof(int));

	int moves;
	float avg_dist;

	for (i=0; i<km->max_iterations; i++) {
		moves = assign_data_to_centroids(data->input, data->num_data, km, centroid_indices);
		printf("vectors moved = %d\n", moves);
		avg_dist = move_centroids(data->input, data->num_data, km, centroid_indices);
		printf("average centroid adjustment = %f\n", avg_dist);

		if (moves <= km->max_moves) break;
		if (avg_dist <= km->max_avg_centroid_correction) break;
	}

	free(centroid_indices);
}



/* allocate a new k_means structure with zeroes in the mean vectors
 *
 */
struct k_means *k_means_create(int k, int vector_size) {

	struct k_means *new_k_means = malloc(sizeof(struct k_means));

	new_k_means->k = k;
	new_k_means->vector_size = vector_size;
	new_k_means->max_iterations = 1000000;
	new_k_means->max_avg_centroid_correction = 0;
	new_k_means->max_moves = 0;

	/* allocate memory for the k-mean vectors & assign the first data vectors */
	new_k_means->means = calloc(k, sizeof(float *));
	int i;
	for (i=0; i<k; i++) {
		new_k_means->means[i] = calloc(vector_size, sizeof(float));
	}

	return new_k_means;
}

void k_means_save_to_file(struct k_means *km, const char *filename) {

	FILE *f = fopen(filename, "w+");

	fprintf(f, "%d %d\n", km->k, km->vector_size);

	int i, j;
	for (i=0; i<km->k; i++) {
		for (j=0; j<km->vector_size; j++) {
			fprintf(f, "%f ", km->means[i][j]);
		}
		fprintf(f, "\n");
	}

	fclose(f);
}
struct k_means *k_means_load_from_file(char *filename) {

	FILE *f = fopen(filename, "r");
	int k, vector_size;

	fscanf(f, "%d %d\n", &k, &vector_size);

	struct k_means *km = k_means_create(k, vector_size);

	int i, j;
	for (i=0; i<km->k; i++) {
		for (j=0; j<km->vector_size; j++) {
			fscanf(f, "%f ", &km->means[i][j]);
		}
		fscanf(f, "\n");
	}

	fclose(f);

	return km;
}





/* move all centroids
 * return the average movement distance
 */
float move_centroids(float **vecs, int num_vecs, struct k_means *km, int *centroid_indices) {

	float avg_movement = 0;

	int i;
	for (i=0; i<km->k; i++) {
		avg_movement = avg_movement + move_centroid(vecs, num_vecs, km, centroid_indices, i);
	}

	avg_movement = avg_movement / km->k;

	return avg_movement;
}

/* move the centroid k to it's new location - the average of those data assigned to it
 * return how far it moved (magnitude of vector)
 */
float move_centroid(float **vecs, int num_vecs, struct k_means *km, int *centroid_indices, int index) {

	int i, j;

	int group_population = 0;

	float *new_centroid = calloc(num_vecs, sizeof(float));
	/* for each data vector */
	for (i=0; i<num_vecs; i++) {

		/* if the vector is in this centroid */
		if (centroid_indices[i] == index) {

			for (j=0; j<km->vector_size; j++)
				new_centroid[j] = new_centroid[j] + vecs[i][j];

			group_population = group_population + 1;
		}

	}
	/* calculate the new average = center of these vectors */
	for (i=0; i<num_vecs; i++) {
		new_centroid[i] = new_centroid[i] / (float)group_population;
	}

	float distance = vector_distance(km->means[index], new_centroid, km->vector_size);

	for (i=0; i<km->vector_size; i++) {
		km->means[index][i] = new_centroid[i];
	}

	free(new_centroid);

	return distance;
}




/* for each data vector, figure out which centroid is closest */
int assign_data_to_centroids(float **vecs, int num_vecs, struct k_means *km, int *centroid_indices) {

	int i, j;
	int moves = 0;

	for (i=0; i<num_vecs; i++) {

		int new_index = find_centroid_for_vector(km, vecs[i]);

		if (centroid_indices[i] != new_index) {
			centroid_indices[i] = new_index;
			moves = moves + 1;
		}
	}

	return moves;
}


int find_centroid_for_vector(struct k_means *km, float *vector) {

	float min_distance = vector_distance(vector, km->means[0], km->vector_size);
	int j, index = 0;

	for (j=1; j<km->k; j++) {
		float current_dist = vector_distance(vector, km->means[j], km->vector_size);
		if (current_dist < min_distance) {
			min_distance = current_dist;
			index = j;
		}
	}
	return index;
}



float vector_distance(float *vec1, float *vec2, int vector_size) {
	int i;
	float dist = 0;
	for (i=0; i<vector_size; i++) {
		dist = dist + (vec1[i]-vec2[i])*(vec1[i]-vec2[i]);
	}
	return sqrt(dist);
}


void k_means_destroy(struct k_means *km) {
	int i;
	for (i=0; i<km->k; i++) {
		free(km->means[i]);
	}
	free(km->means);
	free(km);
}


