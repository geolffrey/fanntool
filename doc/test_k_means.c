#include "fann.h"
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "include/k-means.h"


int main(int argc, char *argv[])
{
	struct fann_train_data *data = fann_read_train_from_file("som_train.data");

	struct k_means *km = k_means_create(30, data->num_input);
	k_means_partition_fann_train_data(km, data);

	k_means_save_to_file(km, "k_means.net");
	struct k_means *km2 = k_means_load_from_file("k_means.net");
	k_means_save_to_file(km2, "k_means2.net");

	k_means_destroy(km);
	k_means_destroy(km2);

	printf("done testing.\n");
	return 0;
}


