#include "fann.h"
#include <string.h>
#include <stdio.h>
#include <math.h>

#ifdef __cplusplus
extern "C"
{

#ifndef __cplusplus
} /* to fool automatic indention engines */
#endif
#endif	/* __cplusplus */

#ifndef NULL
#define NULL 0
#endif	/* NULL */

struct k_means {
	int k;
	float **means;
	int vector_size;
	/* relaxation parameters */
	int max_iterations;
	int max_moves;
	float max_avg_centroid_correction;
};

struct k_means *k_means_create(int k, int vector_size);
void k_means_destroy(struct k_means *km);

void k_means_save_to_file(struct k_means *km, const char *filename);
struct k_means *k_means_load_from_file(char *filename);

int assign_data_to_centroids(float **vecs, int num_vecs, struct k_means *km, int *centroid_indices);
int find_centroid_for_vector(struct k_means *km, float *vector);

float move_centroids(float **vecs, int num_vecs, struct k_means *km, int *centroid_indices);
float move_centroid(float **vecs, int num_vecs, struct k_means *km, int *centroid_indices, int index);

void k_means_partition_fann_train_data(struct k_means *km, struct fann_train_data *data);

float vector_distance(float *vec1, float *vec2, int vector_size);

#ifdef __cplusplus
#ifndef __cplusplus
/* to fool automatic indention engines */
{

#endif
}
#endif	/* __cplusplus */
